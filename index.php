<?php
require 'vendor/autoload.php';
use mywishlist\controler\ControleurListe;
use mywishlist\controler\ControleurItem;
use mywishlist\controler\ControleurGestionListe;
use mywishlist\controler\ControleurGestionItem;
use mywishlist\controler\ControleurIndex;
use Illuminate\Database\Capsule\Manager as DB;
$app = new \Slim\Slim();
$db = new DB();
$db->addConnection(parse_ini_file('src/config/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
//affichage de toute les listes
$app->get('/', function(){
  $rep = new ControleurIndex();
  $rep->afficherAccueil();
})->name('route_accueil');

$app->post('/', function(){
  $rep = new ControleurIndex();
  $rep->afficherListe();
});
//affichage d'un item par son id
$app->get('/:token/afficherItem/:id', function($token ,$id){
  $rep = new ControleurItem();
  $rep->afficherItem($token ,$id);
})->name('route_afficherItem');
//afficher une liste par son id
$app->get('/afficherListe/:token', function($token){
    $rep = new ControleurListe();
    $rep->afficherListe($token);
})->name('route_afficherListe');
//creer une liste
$app->get('/creerListe', function(){
  $rep = new ControleurGestionListe();
  $rep->creerListe();
})->name('route_creerListe');

$app->post('/creerListe', function(){
  $rep = new ControleurGestionListe();
  $rep->savelist();
});
//ajouter un item dans une liste
$app->get('/:token/ajouterItem', function($token){
  $rep = new ControleurGestionItem();
  $rep->ajouterItem($token);
})->name('route_AjoutItem');

$app->post('/:token/ajouterItem', function(){
  $rep = new ControleurGestionItem();
  $rep->saveItem();
});
//reserver un item dans une liste
$app->get('/:token/reserverItem/:id', function($token, $id){
  $rep = new ControleurGestionItem();
  $rep->reserverItem($id);
})->name('route_ReserverItem');

$app->post('/:token/reserverItem/:id', function(){
  $rep = new ControleurGestionItem();
  $rep->saveReservationItem();
});
//partager une liste (recevoir un lien de partage)
$app->get('/:token/partagerListe',function($token){
  $rep = new ControleurListe();
  $rep->partagerListe($token);
})->name('route_partageListe');
//modification  d'une liste
$app->get('/modifierListe/:token', function($token){
  $rep = new ControleurGestionListe();
  $rep->modifierListe($token);
})->name('route_modifListe');

$app->post('/modifierListe/:token', function(){
  $rep = new ControleurGestionListe();
  $rep->savelist();
});
//supprimer une liste
$app->get('/supprimerListe/:token', function($token){
  $rep = new ControleurGestionListe();
  $rep->supprimerListe($token);
})->name('route_suppressionListe');

$app->post('/supprimerListe/:token', function(){
  $rep = new ControleurGestionListe();
  $rep->supprimerListe();
});
$app->run();
