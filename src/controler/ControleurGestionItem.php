<?php
namespace mywishlist\controler;
use mywishlist\models\Liste;
use mywishlist\vue\VueCreation;
use mywishlist\vue\VueParticipant;
use mywishlist\models\Item;
use mywishlist\models\Commentaire;

class ControleurGestionItem{
  //lorsque l'on ajoute un item dans une liste (vers le formulaire)
  public function ajouterItem($token){
    $rep = Liste::select('token')->where('token', '=', $token)->first();
    if(isset($rep)){
      $vue = new VueCreation($token);
      $vue->render('3');
    }
    else{
      echo 'LIEN VERS LISTE INEXISTANTE';
    }
  }

//lorsque l'on enregistre la création dun item
  public function saveItem(){
    // on vérifie que les champs ont été remplis
    if(isset($_POST['valider']) && $_POST['valider']=='val1'){
      if(!empty($_POST['nom']) && !empty($_POST['description']) && !empty($_POST['prix'])){
        $item = new Item;
        $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $item->descr = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
        $item->tarif = filter_var($_POST['prix'], FILTER_SANITIZE_STRING);
        $item->liste_id = filter_var($_POST['tok'], FILTER_SANITIZE_STRING);
        //champs non obligatoire
        if(!empty($_POST['url'])){
          $item->url = filter_var($_POST['url'], FILTER_SANITIZE_URL);
        }
        $item->save();
        $v = new VueCreation(Liste::where('no', $item->liste_id)->first()->token);
        $v->render('4');
      }
      else{
        $v = new VueCreation();
        $v->render('3');
      }
    }
  }

//reserver un item
  public function reserverItem($id){
    $vue = new VueParticipant(null, $id);
    $vue->render('4');
  }


  //sauvegarde de la reservation d'un item
  public function saveReservationItem(){
    if(isset($_POST['valider']) && $_POST['valider']=='val2'){
      //on verifie que le champs du nom soit rempli
      if(!empty($_POST['nom'])){
          $item = Item::where('id',filter_var($_POST['id'], FILTER_SANITIZE_STRING))->first();
          //on sauvegarde le nom de reservation dans un cookie pour éviter de le retaper plus tard
          setcookie('NomReservation',filter_var($_POST['nom'], FILTER_SANITIZE_STRING), time()+60*60*24*30 , "/" );
          $item->reserv = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
          $item->save();
      }
      else{
        $v = new VueParticipant(null, $_POST['id']);
        $v->render('4');
      }
      //champs non obligatoire
      if(!empty($_POST['commentaire'])){
        $item = Item::where('id',filter_var($_POST['id'], FILTER_SANITIZE_STRING))->first();
        $comm = new Commentaire;
        $comm->item_id = $item->id;
        $comm->com = filter_var($_POST['commentaire'], FILTER_SANITIZE_STRING);
        $comm->save();
      }
  }
  $app = \Slim\Slim::getInstance();
  $l = Liste::where('no', $item->liste_id)->first();
  $url = $app->urlFor('route_afficherListe',['token'=>$l->tokenPartage]);
  //on redirige vers la liste
  $app->redirect($url);
}

}
