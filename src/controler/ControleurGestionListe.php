<?php
namespace mywishlist\controler;
use mywishlist\vue\VueCreation;
use mywishlist\models\Liste;
use Illuminate\Database\Capsule\Manager as DB;
class ControleurGestionListe{

  public function creerListe(){
    $vue = new VueCreation();
    $vue->render('1');
  }

  public function modifierListe($token){
    $vue = new VueCreation($token, null, null);
    $vue->render('7');
  }

  //sauvegarde de la liste crée
  public function savelist(){
    //On verifie que chaque champs ont ete remplis
    if(isset($_POST['valider']) && $_POST['valider']=='val1'){
      if(!empty($_POST['titre'])){
        if(!empty($_POST['description'])){
          if(!empty($_POST['expiration'])){
            $liste = new Liste;
            $liste->titre = filter_var($_POST['titre'],FILTER_SANITIZE_STRING);
            $liste->description = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
            $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_STRING);
            $tokenModif = bin2hex(random_bytes(32));
            $liste->token = $tokenModif;
            $liste->save();
            $v = new VueCreation($tokenModif, null, null);
            $v->render('2');
          }
          //sinon on renvoie au formulaire de création de la liste
          else{
            $v = new VueCreation();
            $v->render('1');
          }
        }
        else{
          $v = new VueCreation();
          $v->render('1');
        }
      }
      else{
        $v = new VueCreation();
        $v->render('1');
      }
    }
    //partie modification de la liste
    if(isset($_POST['valider']) && $_POST['valider']=='val4'){
      $liste = Liste::where('token',filter_var($_POST['tok'],FILTER_SANITIZE_STRING))->first();
      if(!empty($_POST['titre'])){
        $liste->titre = filter_var($_POST['titre'],FILTER_SANITIZE_STRING);
      }
      if(!empty($_POST['description'])){
        $liste->description = filter_var($_POST['description'],FILTER_SANITIZE_STRING);
      }
      if(!empty($_POST['expiration'])){
        $liste->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_STRING);
      }

      $liste->save();
      $v = new VueCreation($liste->token);
      $v->render('8');

    }
  }
  //supprimer une liste
  public function supprimerListe($token = 'null'){

    if(isset($_POST['valider']) && $_POST['valider']=='val5'){
      $liste = Liste::where('token', $_POST['tok'])->first();
      if(isset($liste->items)){
        $items = $liste->items;
        foreach ($items as $value) {
          if(isset($value->commentaires)){
            $commentaires = $value->commentaires;
            foreach ($commentaires as $val) {
              $val->delete();
            }
          }
          $value->delete();
        }
      }
      $liste->delete();
      $v = new VueCreation();
      $v->render('9');
    }
    else{
      $v = new VueCreation($token, null, null);
      $v->render('10');
    }
  }
}
