<?php
namespace mywishlist\controler;
use mywishlist\vue\VueParticipant;
use mywishlist\vue\VueCreation;
use mywishlist\models\Item;
use mywishlist\models\Liste;

class ControleurItem{
  //affichage d'un item d'une liste
  public function afficherItem($token, $id){
    $lExiste = Liste::where('tokenPartage', $token )->first();
    $lExiste2 = Liste::where('token', $token )->first();
    //s'il sagit d'un participant
    if(isset($lExiste)){
      $item = Item::find($id);
      $v = new VueParticipant($item, $token);
      $v->render('3');
    }
    //s'il s'agit du créateur
    if(isset($lExiste2)){
      $item = Item::find($id);
      $v = new VueCreation($token,null,$item);
      $v->render('11');
    }

  }

}
