<?php
namespace mywishlist\controler;
use mywishlist\vue\VueParticipant;
use mywishlist\vue\VueCreation;
use mywishlist\models\Liste;


class ControleurListe{
  //affichage d'une liste avec ses items
  public function afficherListe($token){
      $liste = Liste::where('token', $token)->first();
      //si affichage pour le createur
      if(isset($liste)){
        //pour indiquer que c'est le navigateur du createur de la liste
        setcookie('Proprio'.$liste->no, 'OUI', time()+60*60*24*30);
        $items = $liste->items;
        $leTout = array(
          "liste" => $liste,
          "items" => $items
        );
        $v = new VueCreation($token, null, $leTout);
        $v->render('5');
      }
      else{
        //si affichage pour un participant
        $liste = Liste::where('tokenPartage', $token)->first();
        if(isset($liste)){
          $items = $liste->items;
          $leTout = array(
            "liste" => $liste,
            "items" => $items
          );
          $v = new VueParticipant($leTout, $token);
          $v->render('2');
        }
        else{
          $v = new VueParticipant();
          $v->render('5');
        }
      }
    }
  //partager une liste
  public function partagerListe($token){
    $liste = Liste::where('token', $token)->first();
    if(!isset($liste->tokenPartage)){
      $tokenPart = bin2hex(random_bytes(16));
      $liste->tokenPartage = $tokenPart;
      $liste->save();
    }
    else {
        $tokenPart = $liste->tokenPartage;
    }
    $v = new VueCreation($liste->token, $tokenPart, null);
    $v->render('6');
  }
}
