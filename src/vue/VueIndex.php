<?php
namespace mywishlist\vue;

class VueIndex{

  //affichage de l'accueil
  private function htmlAccueil(){
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('route_creerListe');

    $aRendre = <<<FIN
    <center><strong><h1>Bienvenue sur mywishlist</h1></strong></center>
    <center><p>mywishlist est une application web qui vous permet de créer des listes de souhaits, vous pouvez les remplir d'items que vous souhaitez avoir,<br>
    vous pourrez ensuite partager votre listes à vos amis (grâce à un lien) afin qu'ils puissent reserver les différents items .</p></center>
    <center><h3> Créez votre liste dès maintenant !!! : </h3></center>
    <center><a href="$url"><strong>creer une liste</strong></a></center>
FIN;
    return $aRendre;
  }




  public function render($select){
    switch ($select){
      case '1':
      $content = $this->htmlAccueil();
      break;
    }
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('route_accueil');
    $html = <<<ROTH
    <!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="$url/css/styleVueIndex.css"/>
      </head>
      <body>
        <header>
          <h1>MYWISHLIST</h1>
        </header>
      <div class="content">
        $content
        </div>
      </body>
    </html>
ROTH;

    echo $html;
  }

}
