<?php
namespace mywishlist\vue;
use mywishlist\models\Liste;

class VueCreation{
  protected $tokenModif;
  protected $tokenPart;
  protected $listeEtItem;

  public function __construct($tok = 'null', $tokPart = 'null', $tab = 'null'){
    $this->tokenModif = $tok;
    $this->tokenPart = $tokPart;
    $this->listeEtItem = $tab;
  }

//affichage d'une liste pour le créateur (avec options sur la liste)
  private function htmlListItemCreateur(){
    $app = \Slim\Slim::getInstance();
    $action = $app->urlFor('route_creerListe');
    $url = $app->urlFor('route_accueil');
    //les items d'une liste en particulier
    $items = $this->listeEtItem['items'];
    $liste = $this->listeEtItem['liste'];
    //on affiche la partie de la liste (nom, description, date d'expiration)
    $partieListe = <<<end
    <div class = "partieListe">
      <h1><center><strong> Liste : $liste->titre </strong></center></h1>
      <h2><center>$liste->description</center></h2><br>
      <p>Expiration : $liste->expiration</p>
    </div>
end;
//si la liste n'a pas d'items
  if(count($items)!=0){
    $partieItem = '<div class="partieItem"><h3>Les items qui composent cette liste :</h3>';
    foreach ($items as $value) {
      $partieItem = $partieItem . <<<end
      <div class="untem">
        <a href="{$app->urlFor('route_afficherItem', ['token' => $liste->token, 'id' => $value->id])}">$value->nom</a><br>
        <img src="$url/img/$value->img" width =150 height = 150/><br>
      </div>
end;
    }
    $partieItem = $partieItem. '</div>';
  }
  else{
    $partieItem="CETTE LISTE N'A PAS D'ITEMS";
  }
  //partie de modification, exclusif au créateur
    $partieModification = "";
    $dateEcheance = Liste::where('token', $this->tokenModif)->first()->expiration;
    if(date('Y-m-d') <= $dateEcheance){
      $partieModification = <<<end
      <div class="partieModification">
        <ul id="options">
          <strong><li>OPTIONS</li>
          <li><a href="{$app->urlFor('route_AjoutItem', ['token' => $liste->token])}">AJOUTER UN ITEM</a></li>
          <li><a href="{$app->urlFor('route_modifListe', ['token' => $liste->token])}">MODIFIER LES INFORMARTIONS DE LA LISTE</a></li>
          <li><a href="{$app->urlFor('route_suppressionListe', ['token' =>$liste->token])}">SUPPRIMER LA LISTE</a></li>
          <li><a href="{$app->urlFor('route_partageListe', ['token'=>$liste->token])}">OBTENIR LE LIEN DE PARTAGE</a></li>
       </strong>
       </ul>
      </div>
end;
   }

    $aRendre = $partieListe.$partieItem.$partieModification;
    return $aRendre;
  }
  //formulaire de création d'une liste
  private function htmlCreerListe(){
    $app = \Slim\Slim::getInstance();
    $action = $app->urlFor('route_creerListe');
    $aRendre = <<<FIN
    <center><h1>Creer une liste </h1></center>
    <form action=$action method = post>
      <label> titre:
        <input type="text" name="titre"/>
      </label>
    <br><br>
      <label> description:
        <input type="text" name="description"/>
      </label>
    <br><br>
      <label> date expiration:
        <input type="date" name="expiration" />
      </label>
    <br><br>
      <button name="valider" value="val1">Valider</button>
    </form>
FIN;
    return $aRendre;
  }

  //Message qui s'affiche après la création d'une liste
  private function htmlApresCreationListe(){
    $app = \Slim\Slim::getInstance();
    return <<<FIN
    <div class="ApresCrea">
      <center><h1>confirmation de la création de votre liste</h1></center>
      <center> <u>accédez à votre liste :</u> <a href="{$app->urlFor('route_afficherListe', ['token'=>$this->tokenModif])}"> ICI</a><center>
    <br>
      <center><u>token de modification :</u> <strong>$this->tokenModif</strong></center>
    </div>
FIN;
    }
    //formulaire d'ajout d'un item
    private function htmlAjoutItem(){
      $app = \Slim\Slim::getInstance();
      $action = $app->urlFor('route_AjoutItem');
      $tok= Liste::select('no')->where('token', '=', $this->tokenModif)->first();
      $aRendre = <<<FIN
      <center><h1>ajouter un item dans la liste</h1></center>
      <form action=$action method = post>
        <label> nom de l'item:
          <input type="text" name="nom"/>
        </label>
      <br>
        <label> description:
          <input type="text" name="description"/>
        </label>
      <br>
        <label> prix:
          <input type="text" name="prix" />
        </label>
      <br>
        <label> URL externe:
          <input type="text" name="url"/>
        </label>
        <input type="hidden" name="tok" value="$tok->no" />
        <br><button name="valider" value="val1">Valider</button>
      </form>
FIN;

      return $aRendre;
    }

    //message qui s'affiche après la création d'un item
    private function htmlApresCreationItem(){
      $app = \Slim\Slim::getInstance();
      return '<h3> L\'item a été crée avec succés</h3><br><a href="'.$app->urlFor('route_afficherListe', ['token'=>$this->tokenModif]).'">retour a la liste</a>';
    }

    //affiche le lien de partage pour les autres personnes
    private function htmlPartagerListe(){
      $app = \Slim\Slim::getInstance();
      return <<<FIN
      <center><h2>Partage de votre liste :
      </h2></center> <br><center> lien de <strong>partage</strong> de votre liste: <strong>{$_SERVER['HTTP_HOST']}{$app->urlFor('route_afficherListe', ["token" => $this->tokenPart])}</strong><center>
      <a href="{$app->urlFor('route_afficherListe', ['token'=>$this->tokenModif])}">retour a la liste</a>
FIN;
    }
    //formulaire de modification des informations de la liste
    private function htmlModifierListe(){
      $app = \Slim\Slim::getInstance();
      $action = $app->urlFor('route_modifListe');
      $liste = Liste::where('token' , $this->tokenModif)->first();
      $aRendre = <<<FIN
      <center><h1>Modifier La liste : $liste->titre </h1></center>
      <form action=$action method = post>
        <label> titre:
          <input type "text" name="titre" value="$liste->titre" />
        </label>
      <br>
        <label> description:
          <input type "text" name="description" value="$liste->description" />
        </label>
      <br>
        <label> date expiration:
          <input type="date" name="expiration" value="$liste->expiration" />
        </label>
        <input type="hidden" name="tok" value="$liste->token" />
      <br>
        <button name="valider" value="val4">Valider</button>
      </form>
FIN;
      return $aRendre;
    }
//message qui s'affiche après la modification des informations d'une liste
    private function htmlApresModificationListe(){
      $app = \Slim\Slim::getInstance();
      return <<<FIN
      <h3>La liste a été modifiée </h3>
      <a href="{$app->urlFor('route_afficherListe', ['token'=>$this->tokenModif])}">retour a la liste</a>
FIN;
    }
    //Message qui s'affiche après la création d'une liste
    private function htmlApresSuppressionListe(){
      $app = \Slim\Slim::getInstance();
      return <<<FIN
      <h3>La liste a été supprimée </h3>
FIN;
    }

    //Confirmation pour la suppression d'une liste
    private function htmlSuppressionListe(){
      $app = \Slim\Slim::getInstance();
      $action = $app->urlFor('route_suppressionListe');
      $aRendre = <<<end
      <h1><center>Suppimer la liste ?</center></h1>
      <p>Êtes-vous sûr de vouloir supprimer cette liste ?</p>
      <form action= $action method = post>
        <br><button name="valider" value="val5">Valider</button>
        <input type="hidden" name="tok" value="$this->tokenModif" />
      </form>
end;
      return $aRendre;
    }
    //Affichage d'un item d'une liste pour le créateur
    private function htmlListUnItemCreation(){
      $app = \Slim\Slim::getInstance();
      $url = $app->urlFor('route_accueil');
      $aRendre = <<<AMBELLOUIS
      <center><h1>{$this->listeEtItem->nom}</h1></center>
      <div class = itemEimage>
        <div id = "item">
          <span>Description : </span>{$this->listeEtItem->descr}<br><br>
          <span>Tarif : </span>{$this->listeEtItem->tarif}<br><br>
          <span>URL (ref)</span> : {$this->listeEtItem->url}
          <br><br>
        </div>
        <img src=$url/img/{$this->listeEtItem->img} width =250 height = 200/>
      </div>
AMBELLOUIS;
    $dateEcheance = Liste::where('token', $this->tokenModif)->first()->expiration;
    if(date('Y-m-d') >= $dateEcheance){
      if(isset($this->listeEtItem->reserv)){
          $aRendre = $aRendre.'<br><br> Cette item a été reservé par <strong>'.$this->listeEtItem->reserv.'</strong> !<br>';
          $comm = $this->listeEtItem->commentaires;
          if(isset($comm)){
            foreach ($comm as $val) {
              $aRendre = $aRendre.'<br><strong> Son commentaire : </strong>'.$val->com;
            }
          }
        }
    }
      return $aRendre;
    }

    public function render($select){
      switch ($select){
        case '1':
        $content = $this->htmlCreerListe();
        break;
        case '2':
        $content = $this->htmlApresCreationListe();
        break;
        case '3':
        $content = $this->htmlAjoutItem();
        break;
        case '4':
        $content = $this->htmlApresCreationItem();
        break;
        case '5':
        $content = $this->htmlListItemCreateur();
        break;
        case '6':
        $content = $this->htmlPartagerListe();
        break;
        case '7':
        $content = $this->htmlModifierListe();
        break;
        case '8':
        $content = $this->htmlApresModificationListe();
        break;
        case '9':
        $content = $this->htmlApresSuppressionListe();
        break;
        case '10':
        $content = $this->htmlSuppressionListe();
        break;
        case '11':
        $content = $this->htmlListUnItemCreation();
        break;
      }
      $app = \Slim\Slim::getInstance();
      $url = $app->urlFor('route_accueil');
      $html = <<<ROTH
      <!DOCTYPE html>
      <html>
        <head>
          <link rel="stylesheet" href="$url/css/styleVueCreation.css" />
        </head>
        <body>
          <header>
            <ul id="nav">
	            <li>MYWISHLIST</li>
	            <li><a href="$url">ACCUEIL</a></li>
            </ul>
          </header>
          <div class="content">
            $content
          </div>
        </body>
      </html>
ROTH;

      echo $html;
    }

  }
