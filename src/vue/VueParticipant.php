<?php
namespace mywishlist\vue;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Commentaire;

class VueParticipant{
  protected $resultat;
  protected $token;
  //constructeur
  public function __construct($tab = 'null', $tok = 'null'){
    $this->resultat = $tab;
    $this->token = $tok;
  }
  //Formulaire pour la réservation d'un item
  private function htmlReserverItem(){
    $app = \Slim\Slim::getInstance();
    $action = $app->urlFor('route_ReserverItem');
    $item = Item::where('id', $this->token)->first();
    $liste = Liste::where('no', $item->liste_id)->first();
    $nomPredef = "";
    if(isset($_COOKIE['NomReservation'])){
      $nomPredef = $_COOKIE['NomReservation'];
    }
    $aRendre = <<<FIN
    <center><h3>Reservation de l'item : </h3></center>
    <div class = formreserv>
    <form action=$action method = post>
    <label> Votre nom:
    <input type "text" name="nom" value="$nomPredef"/>
    </label>
    <br>
    <label> Commentaire
    <input type "text" name="commentaire" placeholder="facultatif"/>
    </label>
    <br>
    <input type="hidden" name="id" value="$item->id" />
    <input type="hidden" name="tok" value="$liste->tokenPartage" />
    <button name="valider" value="val2">Valider</button>
    </form>
    </div>
FIN;
    return $aRendre;
  }

  //Affichage de toutes les listes (non utilisé dans le projet)
  private function htmlListL(){
    $arendre ='<center><h1>Liste des listes de souhaits </h1></center><br>';
    foreach ($this->resultat as $value ) {
      $arendre = $arendre. <<<VONDERSCHER
      <h3>liste : {$value->titre} </h3> <br>
      description : {$value->description}<br>
      date d'expiration : {$value->expiration}<br>
VONDERSCHER;
    }
    return $arendre;
  }

  //Affichage des items d'une liste
  private function htmlListItem(){
    $items = $this->resultat['items'];
    $liste = $this->resultat['liste'];
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('route_accueil');
    $partieListe = <<<end
    <div class = "partieListe">
    <h1><center><strong> Liste $liste->titre </strong></center></h1>
    <h2><center>$liste->description</center></h2><br>
    <p>Expiration : $liste->expiration</p>
    </div>
end;

    if(count($items)!=0){
      $partieItem = '<div class="partieItem"><h3>Les items qui composent cette liste :</h3>';
      foreach ($items as $value) {
        $partieItem = $partieItem . <<<end
        <div class="untem">
        <a href="{$app->urlFor('route_afficherItem', ['token' => $liste->tokenPartage, 'id' => $value->id])}">$value->nom</a>
        <img src="$url/img/$value->img" width =150 height = 150/><br>
        </div>
end;
      }
      $partieItem = $partieItem. '</div>';
    }
    else{
      $partieItem="CETTE LISTE N'A PAS D'ITEMS";
    }
    $aRendre = $partieListe.$partieItem;
    return $aRendre;
  }

  //afficher l'item d'une liste en particulier
  private function htmlListUnItem(){
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('route_accueil');
    $aRendre = <<<AMBELLOUIS
    <center><h1>{$this->resultat->nom}</h1></center>
    <div class = itemEimage>
    <div id = "item">
    <span>Description : </span>{$this->resultat->descr}<br><br>
    <span>Tarif : </span>{$this->resultat->tarif}<br><br>
    <span>URL (ref)</span> : {$this->resultat->url}
    <br><br>
    </div>
    <img src=$url/img/{$this->resultat->img} width =250 height = 200/>
    </div>
AMBELLOUIS;
    if(!isset($_COOKIE['Proprio'.$this->resultat->liste_id])){
      if(isset($this->resultat->reserv)){
        $aRendre = $aRendre.'<br><br> Cette item a été réservé par '.$this->resultat->reserv;
      }
      else{
        $echeance = Liste::where('tokenPartage', $this->token)->first()->expiration;
        if(date('Y-m-d') < $echeance){
          $aRendre = $aRendre.'<br><br><div id="reserv"> <a href="'.$app->urlFor('route_ReserverItem', ['token' => $this->token, 'id'=>$this->resultat->id]).'">Reserver l\'item</a></div>';
        }
      }
    }
    return $aRendre;
  }

  private function htmlListeInexistante(){
    return '<h2><center>Liste inexistante</center></h2>';
  }



  public function render($select){
    switch ($select){
      case '1':
      $content = $this->htmlListL();
      break;
      case '2':
      $content = $this->htmlListItem();
      break;
      case '3':
      $content = $this->htmlListUnItem();
      break;
      case '4':
      $content = $this->htmlReserverItem();
      break;
      case '5':
      $content = $this->htmlListeInexistante();
      break;
    }
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('route_accueil');
    $html = <<<ROTH
    <!DOCTYPE html>
    <html>
    <head>
    <link rel="stylesheet" href="$url/css/styleVueParticipant.css" />
    </head>
    <body>
    <header>
    <ul id="nav">
    <li>MYWISHLIST</li>
    <li><a href="$url">ACCUEIL</a></li>
    </ul>
    </header>
    <div class="content">
    $content
    </div>
    </body>
    </html>
ROTH;

    echo $html;
  }

}
