<?php

namespace mywishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model {
  protected $table = 'item';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function liste(){
    $s = 'liste_id';
    return $this->belongsTo('\mywishlist\models\Liste', $s);
  }

  public function commentaires(){
    $s = 'item_id';
    return $this->hasmany('\mywishlist\models\Commentaire', $s);
  }
}
