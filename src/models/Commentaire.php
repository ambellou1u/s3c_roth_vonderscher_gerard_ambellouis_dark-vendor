<?php

namespace mywishlist\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model {
  protected $table = 'commentaire';
  protected $primaryKey = 'idCom';
  public $timestamps = false;

  public function item(){
    $s = 'item_id';
    return $this->belongsTo('\mywishlist\models\Item', $s);
  }
}
