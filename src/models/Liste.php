<?php
namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model{
  protected $table="liste";
  protected $primaryKey="no";
  public $timestamps = false ;

  public function items(){
    $s = 'liste_id';
    return $this->hasmany('\mywishlist\models\Item', $s);
  }
}
